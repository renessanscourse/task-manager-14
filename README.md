# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Исключение при пустых id, name, команде | https://yadi.sk/i/_vpqJbBYull82g | 
| Исключение при ошибке в index, комманде | https://yadi.sk/i/9tS3yv2noog-og  |