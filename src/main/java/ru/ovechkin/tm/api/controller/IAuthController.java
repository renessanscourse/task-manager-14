package ru.ovechkin.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

    void showProfileInfo();

    void updateProfileInfo();

    void updatePassword();

}