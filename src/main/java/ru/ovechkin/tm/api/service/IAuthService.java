package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.User;

public interface IAuthService {

    String getUserId();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

    User findUserByUserId(String id);

    void updateProfileInfo(
            String newLogin,
            String newFirstName,
            String newMiddleName,
            String  newLastName,
            String newEmail
    );

    void updatePassword(String currentPassword,String newPassword);

}