package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
