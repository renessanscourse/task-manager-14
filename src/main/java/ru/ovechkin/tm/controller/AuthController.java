package ru.ovechkin.tm.controller;

import ru.ovechkin.tm.api.controller.IAuthController;
import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void showProfileInfo() {
        final String userId = authService.getUserId();
        final User user = authService.findUserByUserId(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PROFILE INFORMATION]");
        System.out.println("YOUR LOGIN IS: " + user.getLogin());
        System.out.println("YOUR FIRST NAME IS: " + user.getFirstName());
        System.out.println("YOUR MIDDLE NAME IS: " + user.getMiddleName());
        System.out.println("YOUR LAST NAME IS: " + user.getLastName());
        System.out.println("YOUR EMAIL IS: " + user.getEmail());
        System.out.println("YOUR ROLE IS: " + user.getRole());
        System.out.println("[OK]");
    }

    @Override
    public void updateProfileInfo() {
        final String userId = authService.getUserId();
        final User user = authService.findUserByUserId(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[UPDATE PROFILE INFO]");
        System.out.println("YOUR CURRENT LOGIN IS: " + "[" + user.getLogin() + "]");
        System.out.print("ENTER NEW LOGIN: ");
        final String newLogin = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT FIRST NAME IS: " + "[" + user.getFirstName() + "]");
        System.out.print("ENTER NEW FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT MIDDLE NAME IS: " + "[" + user.getMiddleName() + "]");
        System.out.print("ENTER NEW MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT LAST NAME IS: " + "[" + user.getLastName() + "]");
        System.out.print("ENTER NEW LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT EMAIL IS: " + "[" + user.getEmail() + "]");
        System.out.print("ENTER NEW EMAIL NAME: ");
        final String newEmail = TerminalUtil.nextLine();
        authService.updateProfileInfo(newLogin, newFirstName, newMiddleName, newLastName, newEmail);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updatePassword() {
        final String userId = authService.getUserId();
        final User user = authService.findUserByUserId(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PASSWORD CHANGE]");
        System.out.print("ENTER YOUR CURRENT PASSWORD: ");
        final String currentPassword = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PASSWORD: ");
        final String newPassword = TerminalUtil.nextLine();
        authService.updatePassword(currentPassword, newPassword);
    }

}