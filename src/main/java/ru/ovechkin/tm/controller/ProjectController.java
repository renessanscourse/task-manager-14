package ru.ovechkin.tm.controller;

import ru.ovechkin.tm.api.controller.IProjectController;
import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(
            final IProjectService projectService,
            final IAuthService authService
    ) {
        this.projectService = projectService;
        this.authService = authService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final String userId = authService.getUserId();
        final List<Project> projects = projectService.findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authService.getUserId();
        projectService.removeAllProjects(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER PROJECT INDEX: ");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = authService.getUserId();
        final Project project = projectService.findProjectByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findProjectByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT INDEX: ");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = authService.getUserId();
        final Project project = projectService.findProjectByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeProjectById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT INDEX: ");
        final Integer index = TerminalUtil.nextNumber() -1;
        final String userId = authService.getUserId();
        final Project project = projectService.removeProjectByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Project project = projectService.removeProjectByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}