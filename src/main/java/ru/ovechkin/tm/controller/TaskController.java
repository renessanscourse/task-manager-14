package ru.ovechkin.tm.controller;

import ru.ovechkin.tm.api.controller.ITaskController;
import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IAuthService authService;

    public TaskController(
            final ITaskService taskService,
            final IAuthService authService
    ) {
        this.taskService = taskService;
        this.authService = authService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final String userId = authService.getUserId();
        final List<Task> tasks = taskService.findAll(userId);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        final String userId = authService.getUserId();
        taskService.removeAllTasks(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        taskService.create(userId, name, description);
        System.out.println("[OK]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER TASK ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER TASK INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Task task = taskService.findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findTaskByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER TASK INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Task task = taskService.findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.print("ENTER NEW TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW TASK DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER TASK ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeTaskById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER TASK INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = authService.getUserId();
        final Task task = taskService.removeTaskByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER TASK NAME: ");
        final String name = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        final Task task = taskService.removeTaskByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}