package ru.ovechkin.tm.exeption.empty;

public class UserEmptyException extends RuntimeException {

    public UserEmptyException() {
        super("Error! User ID is empty...");
    }

}