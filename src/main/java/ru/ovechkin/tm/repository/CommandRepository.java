package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.dto.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CmdConst.CMD_HELP, ArgumentConst.ARG_HELP, "Display terminal commands"
    );

    public static final Command ABOUT = new Command(
            CmdConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info"
    );

    public static final Command VERSION = new Command(
            CmdConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show version info"
    );

    public static final Command INFO = new Command(
            CmdConst.CMD_INFO, ArgumentConst.ARG_INFO, "Display system's info"
    );

    public static final Command HELP_COMMANDS = new Command(
            CmdConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS, "Show available commands"
    );

    public static final Command HELP_ARGUMENTS = new Command(
            CmdConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS, "Show available arguments"
    );

    private static final Command TASK_CREATE = new Command(
            CmdConst.CMD_TASK_CREATE, null, "Create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            CmdConst.CMD_TASK_CLEAR, null, "Remove all tasks"
    );

    private static final Command TASK_LIST = new Command(
            CmdConst.CMD_TASK_LIST, null, "Show task list"
    );

    private static final Command PROJECT_CREATE = new Command(
            CmdConst.CMD_PROJECT_CREATE, null, "Create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            CmdConst.CMD_PROJECT_CLEAR, null, "Remove all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            CmdConst.CMD_PROJECT_LIST, null, "Show project list"
    );

    public static final Command TASK_SHOW_BY_ID = new Command(
            CmdConst.TASK_SHOW_BY_ID, null, "Show task by id"
    );

    public static final Command TASK_SHOW_BY_INDEX = new Command(
            CmdConst.TASK_SHOW_BY_INDEX, null, "Show task by index"
    );

    public static final Command TASK_UPDATE_BY_ID = new Command(
            CmdConst.TASK_UPDATE_BY_ID, null, "Update task by id"
    );

    public static final Command TASK_UPDATE_BY_INDEX = new Command(
            CmdConst.TASK_UPDATE_BY_INDEX, null, "Update task by index"
    );

    public static final Command TASK_REMOVE_BY_ID = new Command(
            CmdConst.TASK_REMOVE_BY_ID, null, "Remove task by id"
    );

    public static final Command TASK_REMOVE_BY_INDEX = new Command(
            CmdConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index"
    );

    public static final Command TASK_REMOVE_BY_NAME = new Command(
            CmdConst.TASK_REMOVE_BY_NAME, null, "Remove task by name"
    );

    public static final Command PROJECT_SHOW_BY_ID = new Command(
            CmdConst.PROJECT_SHOW_BY_ID, null, "Show project by id"
    );

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(
            CmdConst.PROJECT_SHOW_BY_INDEX, null, "Show project by index"
    );

    public static final Command PROJECT_SHOW_BY_NAME = new Command(
            CmdConst.PROJECT_SHOW_BY_NAME, null, "Show project by name"
    );

    public static final Command PROJECT_UPDATE_BY_ID = new Command(
            CmdConst.PROJECT_UPDATE_BY_ID, null, "Update project by id"
    );

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CmdConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index"
    );

    public static final Command PROJECT_REMOVE_BY_ID = new Command(
            CmdConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id"
    );

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CmdConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index"
    );

    public static final Command PROJECT_REMOVE_BY_NAME = new Command(
            CmdConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name"
    );

    public static final Command LOGIN = new Command(
            CmdConst.LOGIN, null, "Login in your account"
    );

    public static final Command REGISTRY = new Command(
            CmdConst.REGISTRY, null, "Register new account"
    );

    public static final Command LOGOUT = new Command(
            CmdConst.LOGOUT, null, "Logout from your account"
    );

    public static final Command SHOW_PROFILE = new Command(
            CmdConst.SHOW_PROFILE, null, "Show information of your account"
    );

    public static final Command UPDATE_PROFILE = new Command(
            CmdConst.UPDATE_PROFILE, null, "Update information of your account"
    );

    public static final Command UPDATE_PASSWORD = new Command(
            CmdConst.UPDATE_PASSWORD, null, "Update password of your account"
    );

    public static final Command EXIT = new Command(
            CmdConst.CMD_EXIT, null, "Close application"
    );

    private final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, HELP_COMMANDS, HELP_ARGUMENTS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME, PROJECT_SHOW_BY_ID,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            LOGIN, REGISTRY, LOGOUT, SHOW_PROFILE, UPDATE_PROFILE, UPDATE_PASSWORD,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}