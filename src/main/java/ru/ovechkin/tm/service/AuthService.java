package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.NotLoggedInException;
import ru.ovechkin.tm.exeption.WrongCurrentPasswordException;
import ru.ovechkin.tm.exeption.empty.LoginEmptyException;
import ru.ovechkin.tm.exeption.empty.PasswordEmptyException;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        if (userId == null) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    public User findUserByUserId(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return userService.findById(userId);
    }

    @Override
    public void updateProfileInfo(
            final String newLogin,
            final String newFirstName,
            final String newMiddleName,
            final String newLastName,
            final String newEmail
    ) {
        final String userId = getUserId();
        final User user = findUserByUserId(userId);
        if (user == null) throw new UserEmptyException();
        if (newLogin != null && !newLogin.isEmpty()) {
            user.setLogin(newLogin);
        }
        if (newFirstName != null && !newFirstName.isEmpty()) {
            user.setFirstName(newFirstName);
        }
        if (newMiddleName != null && !newMiddleName.isEmpty()) {
            user.setMiddleName(newMiddleName);
        }
        if (newLastName != null && !newLastName.isEmpty()) {
            user.setLastName(newLastName);
        }
        if (newEmail != null && !newEmail.isEmpty()) {
            user.setEmail(newEmail);
        }
    }

    @Override
    public void updatePassword(final String currentPassword, final String newPassword) {
        final String userId = getUserId();
        final User user = findUserByUserId(userId);
        if (!HashUtil.salt(currentPassword).equals(user.getPasswordHash())) {
            throw new WrongCurrentPasswordException();
        }
        final String newPasswordHash = HashUtil.salt(newPassword);
        user.setPasswordHash(newPasswordHash);
    }

}